require_relative 'lib/nesnub/version'

Gem::Specification.new do |spec|
  spec.name          = 'nesnub'
  spec.version       = Nesnub::VERSION
  spec.authors       = ['Quien Sabe']
  spec.email         = ['qs5779@mail.com']

  spec.summary       = %q{A gem to help with puppet module testing.}
  spec.description   = %q{A gem to facilitate puppet module testing with vagrant managed virtual machines and serverspec.}
  spec.homepage      = 'https://gitlab.com/wtfo-guru/nesnub'
  spec.license       = "MIT"
  spec.required_ruby_version = Gem::Requirement.new('>= 2.3.0')

  # spec.metadata["allowed_push_host"] = "TODO: Set to 'http://mygemserver.com'"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = 'https://gitlab.com/wtfo-guru/nesnub'
  spec.metadata["changelog_uri"] = 'https://gitlab.com/wtfo-guru/nesnub/-/blob/master/CHANGELOG.md'

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']
end
